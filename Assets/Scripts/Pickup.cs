﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class PickupEvent : UnityEvent<Pickup> { }

[RequireComponent(typeof(BoxCollider))]
public class Pickup : MonoBehaviour
{
    public PickupEvent OnInteract = new PickupEvent();

    private void Start()
    {
        GetComponent<BoxCollider>().isTrigger = true;
        OnInteract.AddListener(OnPickup);
    }

    private void OnTriggerEnter(Collider other)
    {
        ConstanceMovementController cpm = other.GetComponent<ConstanceMovementController>();

        if (cpm != null)
            OnInteract.Invoke(this);
    }

    protected virtual void OnPickup (Pickup item)
    {
        Destroy(gameObject);
    }
}
