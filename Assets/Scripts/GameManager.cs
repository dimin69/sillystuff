﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [SerializeField]
    List<Pickup> pickups = new List<Pickup>();

    public int score;

    private void Start()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        foreach (Pickup item in pickups)
        {
            if (item != null)
                item.OnInteract.AddListener(OnItemCollected);
        }
    }

    void OnItemCollected(Pickup item)
    {
        score += 5;
        pickups.Remove(item);
        Debug.Log("score: " + score);

        if (pickups.Count <= 0)
            SceneManager.LoadScene(2);
    }

}
