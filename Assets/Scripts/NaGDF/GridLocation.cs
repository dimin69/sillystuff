﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
public class GridLocation
{

    Vector3 worldPosistion;

    public GridLocation(Vector3 worldPosistion)
    {
        this.worldPosistion = worldPosistion;
    }

    public Vector3 GetWorldPosistion()
    {
        return worldPosistion;
    }
}*/

public struct GridLocation
{
    Vector3 worldPosistion;

    public GridLocation(Vector3 worldPosistion)
    {
        this.worldPosistion = worldPosistion;
    }

    public Vector3 GetWorldPosistion()
    {
        return worldPosistion;
    }
}