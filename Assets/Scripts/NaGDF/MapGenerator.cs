﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;
using Unity.Jobs;
using System;

public struct CreateGridJob : IJob
{
    int gridAmountX, gridAmountZ;
    public Vector3 worldSize;
    public float gridSlotSize;
    public GridLocation[,] gridSlots;
    Vector3 gridZero;

    public CreateGridJob(Vector3 gridZero, int gridAmountX, int gridAmountZ, Vector3 worldSize, float gridSlotSize, GridLocation[,] gridSlots)
    {
        this.gridAmountX = gridAmountX;
        this.gridAmountZ = gridAmountZ;
        this.worldSize = worldSize;
        this.gridSlotSize = gridSlotSize;
        this.gridSlots = gridSlots;
        this.gridZero = gridZero;
    }

    public void Execute()
    {
        gridAmountX = Mathf.RoundToInt(worldSize.x / gridSlotSize);
        gridAmountZ = Mathf.RoundToInt(worldSize.z / gridSlotSize);

        for (int x = 0; x < gridAmountX; x++)
        {
            for (int y = 0; y < gridAmountZ; y++)
            {
                gridSlots[x , y] = new GridLocation(gridZero + (Vector3.right * ((gridSlotSize * x) + (gridSlotSize * .5f))) + (Vector3.forward * ((gridSlotSize * y) + (gridSlotSize * .5f))));
            }
        }
    }
}


public class MapGenerator : MonoBehaviour
{
    [SerializeField]
    Vector3 WorldSize;
    [SerializeField]
    Vector3 offset;

    [SerializeField]
    [Range(.3f, 10)]
    float gridSlotSize = 5f;

    int gridAmountX, gridAmountZ;


    GridLocation[,] gridSlots;

    Vector3 gridzero;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(CreateGrid());
    }

    private void OnValidate()
    {
        StartCoroutine(CreateGrid());
    }


    [ContextMenu("test")]
    public IEnumerator CreateGrid()
    {
        Debug.Log("testing");

        gridAmountX = Mathf.RoundToInt(WorldSize.x / gridSlotSize);
        gridAmountZ = Mathf.RoundToInt(WorldSize.z / gridSlotSize);

        gridzero = transform.position - (Vector3.right * WorldSize.x * .5f + -Vector3.back * WorldSize.z * .5f);

        //gridSlots = new NativeArray<GridLocationEnum>(gridAmountX + gridAmountZ, Allocator.Persistent);

        NativeArray<GridLocation> result = new NativeArray<GridLocation>(gridAmountX + gridAmountZ, Allocator.Persistent);

        CreateGridJob jobData = new CreateGridJob(gridzero, gridAmountX, gridAmountZ, WorldSize, gridSlotSize, jobData);
        JobHandle handle = jobData.Schedule();


        while (!handle.IsCompleted)
        {
            Debug.Log("test");
            yield return new WaitForEndOfFrame();
        }

        handle.Complete();

        gridSlots = jobData.gridSlots;
        result.Dispose();

        yield return null;

        /*
        for (int x = 0; x < gridAmountX; x++)
        {
            for (int y = 0; y < gridAmountZ; y++)
            {
                gridSlots[x + y] = new GridLocationEnum( new GridLocation(gridzero + (Vector3.right * ((gridSlotSize * x) + (gridSlotSize * .5f))) + (Vector3.forward * ((gridSlotSize * y) + (gridSlotSize * .5f)))));
            }
        }
        */
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position - offset, WorldSize);


        if (gridzero != null)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawCube(gridzero, Vector3.one * 2);
        }


        Gizmos.color = Color.red;
        if (gridSlots != null)
            foreach (GridLocation _gridSlot in gridSlots)
            {
                // if (_gridSlot != null)
                Gizmos.DrawCube(_gridSlot.GetWorldPosistion(), Vector3.one * (gridSlotSize - .2f));
            }
    }

    void NativeArrayToArray(NativeArray<GridLocation> gridArray)
    {
        Debug.Log(gridArray.Length);
        gridSlots = new GridLocation[gridAmountX, gridAmountZ];

        for (int x = 0; x < gridAmountX; x++)
        {
            for (int y = 0; y < gridAmountZ; y++)
            {
                gridSlots[x, y] = gridArray[x + y];
            }
        }
    }
}
