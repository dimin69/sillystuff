﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class AgentController : MonoBehaviour
{
    [SerializeField]
    Transform targetPosistion;

    [SerializeField]
    float startDelay;

    NavMeshAgent ag;

   

    // Use this for initialization
    void Start()
    {
        ag = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (startDelay < 0)
        {
            ag.destination = targetPosistion.position;
        }
        else
            startDelay -= Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<ConstanceMovementController>() != null)
            SceneManager.LoadScene(3); 
    }
}
