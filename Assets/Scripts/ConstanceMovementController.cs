﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum MoveDirection
{
    forward,
    left,
    right,
    backward,

}

[RequireComponent(typeof(Rigidbody))]
public class ConstanceMovementController : MonoBehaviour
{
    Rigidbody rb;
    MoveDirection md;
    MoveDirection oldMd;

    float lastMoveUpdate;

    [SerializeField]
    float speed = 5f;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

        ValidMoveCheck();
        InputHandler();

        switch (md)
        {
            case MoveDirection.forward:
                rb.velocity = transform.forward * speed * Time.deltaTime;
                break;

            case MoveDirection.left:
                rb.velocity = transform.right * -speed * Time.deltaTime;
                break;

            case MoveDirection.right:
                rb.velocity = transform.right * speed * Time.deltaTime;
                break;

            case MoveDirection.backward:
                rb.velocity = transform.forward * -speed * Time.deltaTime;
                break;

            default:
                break;
        }

    }

    void InputHandler()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float Vertical = Input.GetAxis("Vertical");

        if (horizontal > 0.1f)
        {
            OnStateSwitch();
            md = MoveDirection.right;
        }
        else if (horizontal < -.1f)
        {
            OnStateSwitch();
            md = MoveDirection.left;
        }
        else if (Vertical > .1f)
        {
            OnStateSwitch();
            md = MoveDirection.forward;
        }
        else if (Vertical < -.1f)
        {
            OnStateSwitch();
            md = MoveDirection.backward;
        }


    }

    void OnStateSwitch()
    {
        lastMoveUpdate = 1f;
        oldMd = md;
    }


    void ValidMoveCheck()
    {
        if (lastMoveUpdate < 0)
        {
            if (rb.velocity.magnitude < speed)
            {
                md = oldMd;
            }
        }
        else
            lastMoveUpdate -= Time.deltaTime;

    }
}
