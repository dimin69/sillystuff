﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace dimin69.UI
{
    public class SwitchScene : MonoBehaviour
    {
        [SerializeField]
        int lvlNumberToLoad;

        private void Start()
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        public void LoadLevel()
        {
            SceneManager.LoadScene(lvlNumberToLoad);
        }
    }
}
